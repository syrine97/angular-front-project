import { Component, OnInit,NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './_services/login.service'
import * as firebase from 'firebase';
import { isNull } from 'util';


const config = {
  apiKey: 'AIzaSyAsS3w3TWc35QYY7C68LXIYmREH6oPeKOc',
  authDomain: 'angular-maps-277002.firebaseapp.com',
  databaseURL: 'https://angular-maps-277002.firebaseio.com/',
  projectId: 'angular-maps-277002',
  storageBucket: 'gs://angular-maps-277002.appspot.com',
};
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private roles: string;
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  currentUser=null;
  
  STORAGE_KEY = 'local_todolist'
  constructor(
    
        private router: Router,
        public loginService: LoginService
        ) {
          
        }

  ngOnInit() {
    sessionStorage.clear();
  }
  

  logout() {
    this.loginService.logout();
    window.location.reload();
  }
}

