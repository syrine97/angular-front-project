import { Component, OnInit, DefaultIterableDiffer } from '@angular/core';
import { User } from '../users';
import {FormControl, Validators, FormGroup, FormBuilder} from '@angular/forms';
import { ModService } from '../_services/mod.service'
import { StringifyOptions } from 'querystring';
@Component({
  selector: 'app-board-moderator',
  templateUrl: './board-moderator.component.html',
  styleUrls: ['./board-moderator.component.css']
})
export class BoardModeratorComponent implements OnInit {
  addresponse: Object;

  constructor(private modService:ModService) { }
  emailFormControl = new FormControl('', [Validators.required,Validators.email,]);
  Pusername:string;
  Ppassword:string;
  username:string;
 password:string;
 name:string;
 role:string;
 addForm:FormGroup;
 deleted:string;
 added:string;
 updated:string;
 
  ngOnInit(): void {

  }
 
  addUser(event){
    this.username=event.target.username.value;
    this.password=event.target.password.value;
    this.name=event.target.name.value;
    this.role=event.target.role.value;
//
    this.modService.add(this.username,this.password,this.name,this.role)
            .subscribe(
  data => {
   
       console.log('l utilisateur est ajoute')
               this.added='added';
   },
   Error => {
  
  console.log("p ajoute");
   this.added='notadded';
}

 );
  }
   public  deleteUser(event)
   {
    this.username=event.target.username.value;
    this.password=event.target.password.value;
//
    this.modService.delete(this.username,this.password)
            .subscribe(
  data => {
                console.log('l utilisateur est supprime')
               this.deleted='deleted';
},
  Error => {
  
  console.log("operation non reussi");
   this.deleted='notdeleted';
   }
 );
 }
 
  updateUser(event){
    this.Pusername=event.target.Pusername.value;
    this.Ppassword=event.target.Ppassword.value;
    this.username=event.target.username.value;
    this.password=event.target.password.value;
    this.name=event.target.name.value;
    this.role=event.target.role.value;
//
    this.modService.update(this.Pusername,this.Ppassword,this.username,this.password,this.name,this.role)
            .subscribe(
  data => {
   
       console.log('l utilisateur est modifie')
               this.updated='updated';
   },
   Error => {
  
  console.log("p modifie");
   this.updated='notupdated';
}

 );
  }
public displayDelete(){
  if(this.deleted==='deleted')
  return 'utilisateur supprime';
  else
  if(this.deleted==='notdeleted') 
  return 'utilisateur non trouvee'
}  
public displayAdd(){
  if(this.added==='added')
  return 'utilisateur ajoute';
  else
  if(this.added==='notadded') 
  return 'utilisateur n est p ajoute'
}
public displayUpdate(){
  if(this.updated==='updated')
  return 'utilisateur modifie';
  else
  if(this.updated==='notupdated') 
  return 'utilisateur n est p modifie'

}  


}
