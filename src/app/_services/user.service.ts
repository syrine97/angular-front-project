import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  

  getTop5(): Observable<any> {
    return this.http.get('http://localhost:8081/getKPI', {  });
  }

  getAVG(): Observable<any> {
    return this.http.get('http://localhost:8081/getAVG', { });
  }

  getGabs(): Observable<any> {
    return this.http.get('http://localhost:8081/getGabs', { });
  }
}
