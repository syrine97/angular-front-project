import { TestBed } from '@angular/core/testing';

import { ModService } from './_services/mod.service';

describe('ModService', () => {
  let service: ModService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
