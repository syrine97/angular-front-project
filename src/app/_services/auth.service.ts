import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {  BehaviorSubject,Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../users'



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root'})
export class AuthService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    public username:String;
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
   }
   
   public get currentUserValue() {
    return this.currentUserSubject.value;
}

public login(username:string,password:string) {
  return this.http.get('http://localhost:8081/getuser?username='+ username+'&password='+password, {}) 
      .pipe(map(user => {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          
          return user;
      }));
}

logout() {
  // remove user from local storage and set current user to null
  localStorage.removeItem('currentUser');
  this.currentUserSubject.next(null);
}
}