import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModService {
  
  constructor(private http:HttpClient) { }
  public delete(username:string,password:string){
    return this.http.get('http://localhost:8081/deleteUser?username='+ username+'&password='+password, {}) 
      .pipe(map(user => {
          return user;
  }));
}
public add(username:string,password:string,name:string,role:string){
  return this.http.get('http://localhost:8081/addUser?username='+ username+'&password='+password+'&name='+name+'&role='+role, {}) 
    .pipe(map(user => {
        return user;
}));
}
public update(Pusername:string,Ppassword:string,username:string,password:string,name:string,role:string){
  return this.http.get('http://localhost:8081/updateUser?Pusername='+ Pusername+'&Ppassword='+Ppassword+'&username='+ username+'&password='+password+'&name='+name+'&role='+role, {}) 
    .pipe(map(user => {
        return user;
}));
}
public addGab(
  nom:string,longitude:number,latitude:number,etat:string,region:string,rue:string,seuilMin:number,seuilMax:number,soldeRestant:number,tauxActivite:number,tauxReponse:number){
  return this.http.get('http://localhost:8081/addGab?nom='+nom+'&longitude='+longitude+'&latitude='+latitude+'&etat='+etat+'&region='+region+'&rue='+rue+'&seuilMin='+seuilMin+
  '&seuilMax='+seuilMax+'&soldeRestant='+soldeRestant+'&tauxActivite='+tauxActivite+'&tauxReponse='+tauxReponse, {}) 
    .pipe(map(gab => {
        return gab;
}));
}
public deleteGab(
  nom:string,longitude:number,latitude:number){
  return this.http.get('http://localhost:8081/deleteGab?nom='+nom+'&longitude='+longitude+'&latitude='+latitude, {}) 
    .pipe(map(gab => {
        return gab;
}));
}
public updateGab(
  pnom:string,plongitude:number,platitude:number,nom:string,longitude:number,latitude:number,etat:string,region:string,rue:string,seuilMin:number,seuilMax:number,soldeRestant:number,tauxActivite:number,tauxReponse:number){
  return this.http.get('http://localhost:8081/updateGab?pnom='+pnom+'&plongitude='+plongitude+'&platitude='+latitude+'&nom='+nom+'&longitude='+longitude+'&latitude='+latitude+'&etat='+etat+'&region='+region+'&rue='+rue+'&seuilMin='+seuilMin+
  '&seuilMax='+seuilMax+'&soldeRestant='+soldeRestant+'&tauxActivite='+tauxActivite+'&tauxReponse='+tauxReponse, {}) 
    .pipe(map(gab => {
        return gab;
}));
}

}