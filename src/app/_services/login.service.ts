

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {  BehaviorSubject,Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../users'



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root'})
export class LoginService {
  user:User;
  username:string
  constructor(private http: HttpClient) {}

public login(username:string,password:string) {
  return this.http.get('http://localhost:8081/getuser?username='+ username+'&password='+password, {}) 
      .pipe(map(user => {
          
          return user;
      }));
}
isUserLoggedIn() {
  let user = sessionStorage.getItem('username')
  console.log(!(user === null))
  return !(user === null);
}
getName(){
  console.log(sessionStorage.getItem('name'))
  return  sessionStorage.getItem('name');
}

getUserName(){
  console.log(sessionStorage.getItem('username'))
  return  sessionStorage.getItem('username');
  
}
getPassword(){
  console.log(sessionStorage.getItem('password'))
  return  sessionStorage.getItem('password');
  
}
getUserRole(){
  console.log(sessionStorage.getItem('role'))
  return  sessionStorage.getItem('role')==='admin';
  
}

logout() {
  // remove user from local storage and set current user to null
 }
}