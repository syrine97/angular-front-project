import { Component, OnInit,NgModule} from '@angular/core';
import * as L from 'leaflet';
import { UserService } from '../_services/user.service';
import { Gab } from '../gab';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css']
})
export class BoardUserComponent implements OnInit {
  map;
  public  gabs: any[];
  public  top5s: any[];
  public moy:any[];
  public regMoy:any[];
  public gab:Gab;


  // retrieve from https://gist.github.com/ThomasG77/61fa02b35abf4b971390
  smallIcon = new L.Icon({
    iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon.png',
    iconRetinaUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon-2x.png',
    iconSize:    [25, 41],
    iconAnchor:  [12, 41],
    popupAnchor: [1, -34],
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    shadowSize:  [41, 41]
  });

 

  constructor(
    private userService:UserService
  ) { 
    this.gabs=[];
    this.top5s=[];
    this.regMoy=[];
  }

  ngOnInit(): void {
    this.createMap();
    this.displayTop5();
    this.displayAVG();
  }

  createMap() {
    const parcThabor = {
      lat: 34,
      lng: 9,
    };

    const zoomLevel = 7;

    this.map = L.map('map', {
      center: [parcThabor.lat, parcThabor.lng],
      zoom: zoomLevel
    });

    const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      minZoom: 5,
      maxZoom: 20,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    mainLayer.addTo(this.map);
    // try marker
    
   
  //get all gabs
     this.userService.getGabs().subscribe(data => {
      this.gabs = data; 
    console.log(this.gabs)
    console.log('gabs ressu')
    
    console.log('markers');
    for (let g of this.gabs) {
      console.log(g.longitude,g.latitude);
  var marker = L.marker([g.longitude,g.latitude], { icon: this.smallIcon });
  marker.addTo(this.map).bindPopup('gab '+g.gabname+':' +g.etat);
}
    },
    isNull => {
    console.log('gabs non ressu')
    });
    console.log(this.gabs);
    

}

public  displayTop5(){
this.userService.getTop5()
.subscribe(data => {
  // (4) Store
  this.top5s = data; 
console.log(this.top5s)
console.log('top5 ressu')
},
isNull => {
console.log('top5 non ressu')
});
}
public displayAVG(){
  this.userService.getAVG()
  .subscribe( data => {
    this.regMoy=data;
    },
isNull => {

console.log('moy non ressu')
}
);
  }
}





   
 
    