import { Component, OnInit } from '@angular/core';
import { ModService } from '../_services/mod.service'
import {FormControl, Validators, FormGroup, FormBuilder} from '@angular/forms';
import { LoginService } from '../_services/login.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  constructor(private modService:ModService,
    private loginService:LoginService) { }
    
emailFormControl = new FormControl('', [Validators.required,Validators.email,]);
username:string;
password:string;
myUsername:string;
myPassword:string;
name:string;
role:string;
updateForm:FormGroup;
updated:string;
updatedMe:string;
added:string;
deleted:string;
etat:string;
platitude:number;
plongitude:number ;
pnom:string;
latitude:number;
longitude:number ;
nom:string;
region:string;
rue:string;
seuilMin:number;
seuilMax:number;
soldeRestant:number;
tauxActivite:number;
tauxReponse:number;

ngOnInit(): void {
}
previsionCash(event){}
updateMe(event){
this.myUsername=this.loginService.getUserName();
this.myPassword=this.loginService.getPassword();
this.username=event.target.username.value;
this.password=event.target.password.value;
this.name=event.target.name.value;
this.role=event.target.role.value;
//
console.log(this.myUsername,this.myPassword);
this.modService.update(this.myUsername,this.myPassword,this.username,this.password,this.name,this.role)
  .subscribe(
data => {

console.log('l utilisateur est modifie')
     this.updatedMe='updated';
},
Error => {

console.log("p modifie");
this.updatedMe='notupdated';
}

);
}
previsionCashDisplay(){}
displayUpdateMe(){
if(this.updatedMe==='updated')
return 'utilisateur modifie';
else
if(this.updatedMe==='notupdated') 
return 'utilisateur n est p modifie'
}
addGab(event){
  this.nom=event.target.nom.value;
  this.longitude=event.target.longitude.value;
  this.latitude=event.target.latitude.value;
  this.etat=event.target.etat.value;
  this.region=event.target.region.value;
  this.rue=event.target.rue.value;
  this.seuilMin=event.target.seuilMin.value;
  this.seuilMax=event.target.seuilMax.value;
  this.soldeRestant=event.target.soldeRestant.value;
  this.tauxActivite=event.target.tauxActivite.value;
  this.tauxReponse=event.target.tauxReponse.value;
//
  this.modService.addGab(this.nom,this.longitude,this.latitude,this.etat,this.region,this.rue,this.seuilMin,this.seuilMax,this.soldeRestant,this.tauxActivite,this.tauxReponse)
          .subscribe(
data => {
 
     console.log('gab ajoute')
             this.added='added';
 },
 Error => {

console.log("gab n est p ajoute");
 this.added='notadded';
});
}
public deleteGab(event){
  this.nom=event.target.nom.value;
  this.longitude=event.target.longitude.value;
  this.latitude=event.target.latitude.value;
  this.modService.deleteGab(this.nom,this.longitude,this.latitude)
          .subscribe(
data => {
 
     console.log('gab supprimee')
             this.deleted='deleted';
 },
 Error => {

console.log("gab n est p ajoute");
 this.deleted='notdeleted';
});
}
updateGab(event){
  this.pnom=event.target.pnom.value;
  this.plongitude=event.target.plongitude.value;
  this.platitude=event.target.platitude.value;
  this.nom=event.target.nom.value;
  this.longitude=event.target.longitude.value;
  this.latitude=event.target.latitude.value;
  this.etat=event.target.etat.value;
  this.region=event.target.region.value;
  this.rue=event.target.rue.value;
  this.seuilMin=event.target.seuilMin.value;
  this.seuilMax=event.target.seuilMax.value;
  this.soldeRestant=event.target.soldeRestant.value;
  this.tauxActivite=event.target.tauxActivite.value;
  this.tauxReponse=event.target.tauxReponse.value;
//
  this.modService.updateGab(this.pnom,this.plongitude,this.platitude,this.nom,this.longitude,this.latitude,this.etat,this.region,this.rue,this.seuilMin,this.seuilMax,this.soldeRestant,this.tauxActivite,this.tauxReponse)
          .subscribe(
data => {
 
     console.log('gab modifie')
         this.updated='updated';
 },
 Error => {

console.log("gab n est p modifie");
 this.updated='notupdated';
});
}

displayAdd()
{
  if(this.added==='added')
  return 'gab ajoute';
  else
  if(this.added==='notadded') 
  return 'gab n est p ajoute'
}
public displayUpdate(){
  if(this.updated==='updated')
  return 'gab modifie';
  else
  if(this.updated==='notupdated') 
  return 'gab n est p modifie'
}
public displayDelete(){
  if(this.deleted==='deleted')
  return 'gab supprime';
  else
  if(this.deleted==='notdeleted') 
  return 'gab non trouvee'
}  

}
