import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup, FormBuilder} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../_services/login.service';
import { User } from '../users';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup
  fb:FormBuilder
  roles: string[] = [];
  isLoggedIn= false;
    error: string;
    user: any;
    username:string ;
    password:string;

  emailFormControl = new FormControl('', [Validators.required,Validators.email,]);
 
  constructor(private formBuilder:FormBuilder,
              private router:Router,
              private route: ActivatedRoute,
              private loginService: LoginService, 
    ) {
      // redirect to home if already logged in
    }
  ngOnInit(){ 
   sessionStorage.clear();
    this.loginForm=this.formBuilder.group({
       email:['',Validators.required],
       password:['',Validators.required] })
     
  }

  // convenience getter for easy access to form fields
get f() { return this.loginForm.controls; }

  onSubmit(event) {

    this.loginForm=this.formBuilder.group({
      email:[event.target.username.value],
      password:[event.target.password.value]
   })
console.log(event.target.username.value);
console.log(event.target.password.value);
//user form

this.username=event.target.username.value;
this.password=event.target.password.value;
//
this.loginService.login(this.username,this.password)
            .subscribe(
                data => {
                 this.user=data;
              console.log('user')
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              sessionStorage.setItem('name',this.user.name);
              sessionStorage.setItem('role',this.user.roles);
              sessionStorage.setItem('username',this.user.userName);
              sessionStorage.setItem('password',this.user.pass);
              this.router.navigate(['/user']);
  
},


 Error => {
  
  console.log('ressayer')
  alert("User name or password is incorrect")
   }
 );
 

      
    }
  }